/* Tiré de exemples/MPU6050/MPU6050_raw
 *  VCC - 3.3V
 *  GND - GND
 *  SCL - A5
 *  SDA - A4
 *  int roll=0, pitch=0;
 *  roll= atan2((double)ax,(double)az )*180/PI;
 *  pitch= atan2((double)ay,(double)az )*180/PI;
 */
#include "I2Cdev.h"
#include "MPU6050.h"
#include "double_gyro.h"

MPU6050 accelgyro1(0x68);
MPU6050 accelgyro2(0x69); //use for second MPU6050 device where AD0 must be high (3.3V)

int16_t a1x, a1y, a1z;
int16_t g1x, g1y, g1z;
int roll_1, pitch_1;

int16_t a2x, a2y, a2z;
int16_t g2x, g2y, g2z;
int roll_2, pitch_2;

int16_t relative_roll;

void initialization() 
{

    // initialize device
    Serial.println("Initializing I2C devices...");
    accelgyro1.initialize();
    accelgyro2.initialize();

    // verify connection
    Serial.println("Testing device connections...");
    Serial.println(accelgyro1.testConnection() ? "MPU6050 (0x68) connection successful" : "MPU6050 (0x68) connection failed");
    Serial.println(accelgyro2.testConnection() ? "MPU6050 (0x69) connection successful" : "MPU6050 (0x69) connection failed");


}

void calcul() 
{
    accelgyro1.getMotion6(&a1x, &a1y, &a1z, &g1x, &g1y, &g1z);
    roll_1= atan2((double)a1x,(double)a1z )*180/PI;
    pitch_1= atan2((double)a1y,(double)a1z )*180/PI;                                                                                           
    //Serial.print("roll_1= "); Serial.print(roll_1); 
    //Serial.print("  pitch_1= ");Serial.println(pitch_1); 

    accelgyro2.getMotion6(&a2x, &a2y, &a2z, &g2x, &g2y, &g2z);
    roll_2= atan2((double)a2x,(double)a2z )*180/PI-90;
    pitch_2= atan2((double)a2y,(double)a2z )*180/PI;                                                                                           
    //Serial.print("roll_2= "); Serial.print(roll_2); 
    //Serial.print("  pitch_2= ");Serial.println(pitch_2); 
    
    //Serial.println("");
    relative_roll=roll_1-roll_2;
    //Serial.print("relative_roll= "); Serial.println(relative_roll); 
    //Serial.println("");
  
    
//    Serial.print("a/g:\t");
//    Serial.print(ax); Serial.print("\t");
//    Serial.print(ay); Serial.print("\t");
//    Serial.print(az); Serial.print("\t");
//    Serial.print(gx); Serial.print("\t");
//    Serial.print(gy); Serial.print("\t");
//    Serial.println(gz);
    delay(20);
}

int get_pitch()
{ 
  return pitch_1;
}

int get_relative_roll()
{
  return relative_roll;
}




