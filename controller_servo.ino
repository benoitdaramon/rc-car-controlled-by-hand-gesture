#include <Servo.h>
#include "double_gyro.h"
int roll = 0, pitch = 0;
int val_vitesse = 0, val_direction = 0;
Servo servo_speed;
Servo servo_direction;


void setup()
{
    Serial.begin(9600);
    initialization();   //fonction of double_gyro, initialize the gyros
    for (int pinNumber = 5; pinNumber <= 7 ; pinNumber ++ )
    {
        pinMode(pinNumber, OUTPUT);
        digitalWrite(pinNumber, LOW);
    }
    servo_speed.attach(9);
    servo_direction.attach(10);
}


void loop()
{
    calcul();
    roll = get_relative_roll();   //relative, so we have the movement from the hand and not from the arm
    pitch = get_pitch();
	roll = constrain(roll, -90, 90);    //to limit the roll
	pitch = constrain(pitch, -90, 90);    //no need to lower the bound of the direction
    val_vitesse = map(roll, 90, -90, 45, 100);    //values found so the car does not move at roll=0
    val_direction = map(pitch, -90, 90, 70, 155);
	
    Serial.print("val_vitesse="); Serial.print(val_vitesse);
    Serial.print("   val_direction= "); Serial.println( val_direction);

    servo_speed.write(val_vitesse);
    servo_direction.write(val_direction);
    delay(50);
}




